#!/usr/bin/python
from Classes.clickEvent import *
from Classes.pymouse import *
from PIL import Image
from PIL import *
import time
import os

locations = {}
mouse 	  = PyMouse()

print "Welcome to grinder v0.1"

def calibrate():
	click_event = clickEvent()
	click_event.capture = True
	
	print "Set 'spin' button location:" 
	click_event.run()
	locations['spin'] = click_event.point
	print('--> set')
	
	print "Set 'clear' button location:" 
	click_event.restart()
	locations['clear'] = click_event.point
	print('--> set')
	
	print "Set 'money' button location:"
	click_event.restart()
	locations['money'] = click_event.point
	print('--> set')
	
	print "Set 'color' location:"
	click_event.restart()
	locations['color'] = click_event.point
	print('--> set')
	
	print "Set 'bet black' location:"
	click_event.restart()
	locations['bet_black'] = click_event.point
	print('--> set')
	
	print "Set 'bet red' location:"
	click_event.restart()
	locations['bet_red'] = click_event.point
	print('--> set')
	
	del click_event

def getColorFromImage():
	im      = Image.open('printscreen.jpg')
	rgb_im  = im.convert('RGB')
	r, g, b = rgb_im.getpixel((locations['color']["x"] * 2, locations['color']["y"] * 2))

	return r, g, b

def getColorFromRGB(rgb):
	color = ""
	
	r = rgb[0]
	g = rgb[1]
	b = rgb[2]
	
	percent_red   = float(r) / 255 * 100
	percent_green = float(g) / 255 * 100
	
	if percent_red > percent_green:
		color = "red"
	elif percent_green > percent_red:
		color = "green"
	else:
		color = "black"
		
	return color
	
	
def startGrind():
	
	while True:
		time.sleep( 5 )
		mouse.click(locations['spin']["x"], locations['spin']["y"])
		time.sleep( 2 )
		os.system("screencapture printscreen.jpg")
		print getColorFromRGB(getColorFromImage())

calibrate()
startGrind()